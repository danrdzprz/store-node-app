FROM node:14
WORKDIR /devone-app
COPY package.json .
RUN npm install
RUN npm install nodemon -g
COPY . .
# CMD npm start
# CMD npm run dev
# CMD nodemon -L index.js
CMD npm run watch
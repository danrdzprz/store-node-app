// const mysql = require("mysql");
const dbConfig = require("../config/db.config.js");

// var connection = mysql.createPool({
//   host: dbConfig.HOST,
//   user: dbConfig.USER,
//   password: dbConfig.PASSWORD,
//   database: dbConfig.DB
// });

// module.exports = connection;

const mysql = require('mysql2/promise');

const connection = mysql.createPool({
        host: dbConfig.HOST,
        user: dbConfig.USER,
        password: dbConfig.PASSWORD,
        database: dbConfig.DB,
        waitForConnections: true,
        connectionLimit: 10,
        queueLimit: 0

});

module.exports = connection;
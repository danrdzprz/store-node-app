const sql = require("./db.js");

// constructor
const Product = function(product) {
  this.name = product.name;
  this.amount = product.amount;
  this.due_date = product.due_date;
  this.store_id = product.store_id;
};

Product.create = async(new_product, result) => {
  // const data = {
  //   name:new_product.name,
  //   amount:new_product.amount,
  //   due_date: new_product.due_date,
  // }
  // const stores = new_product.store_id;
  // await sql.query("INSERT INTO products SET ?", data, async(err, res) => {
  //   if (err) {
  //     console.log("error: ", err);
  //     result(err, null);
  //   }
  //   for(const store_id of stores) {
  //     const relation = {
  //       product_id: res.insertId,
  //       store_id: store_id
  //     }
  //     await sql.query("INSERT INTO product_store SET ?", relation, (err, res) => {
  //       if (err) {
  //         console.log("error: ", err);
  //         result(err, null);
  //         return;
  //       }
  //       console.log("created relation:");
  //     });
  //   }
  //   console.log("created product: ", res);
  //   console.log("aqui");
  //   result(null,{ id: res.insertId, ...new_product });
  // });
  const data = {
    name:new_product.name,
    amount:new_product.amount,
    due_date: new_product.due_date,
  }
  const stores = new_product.store_id;
  const [newProduct] = await sql.query("INSERT INTO products SET ?", data);

  for(const store_id of stores) {
    const relation = {
      product_id: newProduct.insertId,
      store_id: store_id
    }
    await sql.query("INSERT INTO product_store SET ?", relation);
  }
  result(null,{ id: newProduct.insertId, ...new_product });
};

Product.findById = (id, result) => {
  sql.query(`SELECT * FROM products WHERE id = ${id}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found Product: ", res[0]);
      result(null, res[0]);
      return;
    }

    // not found Product with the id
    result({ kind: "not_found" }, null);
  });
};

Product.getAll = (title, result) => {
  let query = "SELECT * FROM products";

  if (title) {
    query += ` WHERE title LIKE '%${title}%'`;
  }

  sql.query(query, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("products: ", res);
    result(null, res);
  });
};

Product.getAllPublished = result => {
  sql.query("SELECT * FROM products WHERE published=true", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("products: ", res);
    result(null, res);
  });
};

Product.updateById = (id, product, result) => {
  sql.query(
    "UPDATE products SET name = ?, amount = ?, due_date = ? WHERE id = ?",
    [product.name, product.amount, product.due_date, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        // not found product with the id
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated product: ", { id: id, ...product });
      result(null, { id: id, ...product });
    }
  );
};

Product.remove = (id, result) => {
  sql.query("DELETE FROM products WHERE id = ?", id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found product with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted product with id: ", id);
    result(null, res);
  });
};

Product.removeAll = result => {
  sql.query("DELETE FROM products", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log(`deleted ${res.affectedRows} products`);
    result(null, res);
  });
};

module.exports = Product;

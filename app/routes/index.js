const router = require("express").Router();

const product_routes = require("./products.routes.js");
router.use('/products', product_routes);

module.exports = router;

// module.exports = (app) => {
//   // app.use('/products', router);
// };
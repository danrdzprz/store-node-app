const controller = require("../controllers/product.controller.js");

const router = require("express").Router();

/** POST /api/products */
router.post('/',controller.store);
/** GET /api/products */
router.get('/',controller.index);
/** PUT /api/products */
router.put('/:id',controller.update);
/** DELETE /api/products */
router.delete('/:id',controller.remove);
/** GET /api/products */
router.get('/:id',controller.show);

module.exports = router;
-- CREATE DATABASE node_app;
-- USE node_app;
CREATE TABLE products 
(
  id INTEGER UNSIGNED auto_increment , 
  name VARCHAR(255) NOT NULL, 
  amount DECIMAL(6,2) NOT NULL, 
  due_date DATE NOT NULL, 
  PRIMARY KEY (id)
);

CREATE TABLE cities 
(
  id INTEGER UNSIGNED auto_increment , 
  name VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE stores 
(
  id INTEGER UNSIGNED auto_increment , 
  name VARCHAR(255) NOT NULL,
  city_id INTEGER UNSIGNED NOT NULL,
  FOREIGN KEY(city_id) REFERENCES cities(id),
  PRIMARY KEY (id)
);

CREATE TABLE product_store (
  product_id INTEGER UNSIGNED NOT NULL,
  store_id INTEGER UNSIGNED NOT NULL,
  FOREIGN KEY(product_id) REFERENCES products(id),
  FOREIGN KEY(store_id) REFERENCES stores(id)
);

-- CREATE TABLE store_city (
--   city_id INTEGER UNSIGNED NOT NULL,
--   store_id INTEGER UNSIGNED NOT NULL,
--   FOREIGN KEY(city_id) REFERENCES cities(id),
--   FOREIGN KEY(store_id) REFERENCES stores(id),
--   UNIQUE KEY city_store_unique (city_id,store_id)
-- );

INSERT INTO cities (id, name) VALUES (1, 'ciudad_1');
INSERT INTO cities (id, name) VALUES (2, 'ciudad_2');

INSERT INTO stores (id, name, city_id) VALUES (1, 'tienda_1',1);
INSERT INTO stores (id, name, city_id) VALUES (2, 'tienda_2',2);
INSERT INTO stores (id, name, city_id) VALUES (3, 'tienda_3',1);


-- INSERT INTO products (name, amount, due_date) VALUES ('coca', '12.12', '2020-11-11 12:23');
-- INSERT INTO product_store (product_id, store_id) VALUES (1, 1);
-- INSERT INTO product_store (product_id, store_id) VALUES (1, 2);

-- SELECT * FROM products;

-- SELECT * FROM cities;

-- SELECT * FROM stores;

-- select * from products join product_store on products.id = product_store.product_id join stores on stores.id = product_store.store_id;